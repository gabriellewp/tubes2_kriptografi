﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeoCipher
{
    class Cipher
    {
        private byte[] subBox1 = {7,5,2,7,4,0,7,2,
                                         1,3,2,7,3,0,4,4,
                                         6,3,6,5,5,2,7,1,
                                         5,0,3,1,5,1,3,0,
                                         6,0,6,7,5,4,4,3,
                                         1,2,5,2,4,3,7,2,
                                         0,1,4,4,2,0,1,1,
                                         7,3,0,6,6,6,6,5};
        private byte[] subBox2 = {6,4,2,6,2,2,5,7,
                                         7,4,5,4,3,3,7,5,
                                         6,5,3,0,5,1,3,1,
                                         1,5,0,4,7,4,7,1,
                                         6,6,2,6,1,3,7,0,
                                         2,0,7,3,2,7,5,1,
                                         0,4,2,0,0,3,1,3,
                                         4,6,1,0,5,2,4,6};
        private byte[] subBox3 = {5,3,2,2,3,4,0,5,
                                         5,1,2,1,7,1,0,3,
                                         7,4,6,4,4,6,5,7,
                                         6,6,7,2,1,3,0,1,
                                         2,4,0,5,0,4,0,1,
                                         5,4,2,1,3,3,3,6,
                                         7,7,1,0,6,3,0,7,
                                         5,6,2,5,6,7,2,4};
        private byte[] key; //key of the whole cipher
        private byte[] iv; //initial vector for CBC mode
        private int nPad; //number of padding character
        //constructor
        public Cipher(string s){
            string str = s;
            if (str.Length % 2 != 0) str += "0";
            key = Encoding.ASCII.GetBytes(str);
        }
        private bool GetBit(byte source, int idx)
        {
            return (((source >> idx) & 1) == 1);
        }

        private byte SetBit(byte target, bool source, int idx)
        {
            int value = source ? 1 : 0;
            int temp = (value << idx);
            return (byte)((target & ~(1 << idx)) | temp);
        }

        private byte CircularLeftShift(byte toShift, int count)
        {
            int actualCount = count % 8;
            if (actualCount < 0){
                actualCount += 8;
            }
            byte finalResult = (byte)(toShift << actualCount);
            for (int i = 0; i < actualCount; i++)
            {
                finalResult = SetBit(finalResult, GetBit(toShift, 7 - i), actualCount - 1 - i);
            }
            return finalResult;
        }

        private byte CircularRightShift(byte toShift, int count)
        {
            int actualCount = count % 8;
            if (actualCount < 0)
            {
                actualCount += 8;
            }
            byte finalResult = (byte)(toShift >> actualCount);
            for (int i = 0; i < actualCount; i++)
            {
                finalResult = SetBit(finalResult, GetBit(toShift, actualCount - 1 - i), 7 - i);
            }
            return finalResult;
        }

        private byte[] feistelCore(byte[] theText, byte[] theKey, int round)
        {
            int blockSize = theText.Length;
            int halfBlockSize = blockSize / 2;
            int doubBlockSize = blockSize * 2;
            int doubBlockBit = 8 * doubBlockSize;
            byte[] combined = new byte[doubBlockSize];
            byte[] mixed = new byte[doubBlockSize];
            byte[] result = new byte[blockSize];
            theText.CopyTo(combined, 0);
            for (int i = 0; i < blockSize; i++)
            {
                combined[blockSize + i] = theKey[i];
            }
            byte[] usedSubMatrix;
            if ((round == 3) || (round == 5) || (round == 8) || (round == 11) || (round == 14))
            {
                usedSubMatrix = subBox2;
            }
            else if ((round == 1) || (round == 7) || (round == 9) || (round == 12) || (round == 15))
            {
                usedSubMatrix = subBox3;
            }
            else
            {
                usedSubMatrix = subBox1;
            }
            for (int i = 0; i < doubBlockBit; i++)
            {
                int byteNum = i / 8;
                int bitNum = i % 8;
                if (bitNum < 4)
                    mixed[byteNum] = SetBit(mixed[byteNum], GetBit(combined[halfBlockSize + (byteNum / 2)], bitNum), bitNum);
                else
                    mixed[byteNum] = SetBit(mixed[byteNum], GetBit(combined[byteNum], bitNum), bitNum);
            }
            for (int i = 0; i < blockSize; i++)
            {
                result[i] = usedSubMatrix[((int)mixed[2 * i] + (int)mixed[2 * i + 1]) % 64];
                result[i] = (byte)(result[i] ^ theKey[i + blockSize]);
            }
            return result;
        }

        public byte[] E(byte[] plainText)
        {
            int blockSize = plainText.Length;
            int halfBlockSize = blockSize / 2;
            byte[] result = (byte[])plainText.Clone();
            byte[] left = new byte[halfBlockSize];
            byte[] right = new byte[halfBlockSize];
            byte[] temp;

            List<byte[]> internalKeys = generateInternalKeys();

            for (int x = 0; x < blockSize; x++)
            {
                for (int i = 0; i < halfBlockSize; i++)
                {
                    left[i] = result[i];
                    right[i] = result[i + halfBlockSize];
                }
                temp = feistelCore(right, internalKeys[x], x);
                for (int i = 0; i < halfBlockSize; i++)
                {
                    byte tmp = right[i];
                    right[i] = (byte)(temp[i] ^ left[i]);
                    left[i] = tmp;
                }
                for (int i = 0; i < halfBlockSize; i++)
                {
                    result[i] = left[i];
                    result[i + halfBlockSize] = right[i];
                }
            }
            return result;
        }

        private List<byte[]> generateInternalKeys()
        {
            int blockSize = key.Length;
            byte[] oldKey = (byte[])key.Clone();
            byte[] newKey = (byte[])key.Clone();
            List<byte[]> internalKeys = new List<byte[]>();
            for (int i = 0; i < blockSize; i++)
            {
                for (int x = 0; x < blockSize; x++)
                {
                    newKey[x] = (byte)((oldKey[x] + newKey[x]) % 256);
                    if (i % 2 == 0)
                        newKey[x] = CircularLeftShift(newKey[x], x);
                    else
                        newKey[x] = CircularRightShift(newKey[x], x);
                }
                internalKeys.Add(newKey);
                byte[] temp = (byte[])newKey.Clone();
                newKey = oldKey;
                oldKey = temp;
            }
            return internalKeys;
        }

        private byte[] D(byte[] cipherText)
        {
            int blockSize = cipherText.Length;
            int halfBlockSize = blockSize / 2;
            byte[] result = (byte[])cipherText.Clone();
            byte[] left = new byte[halfBlockSize];
            byte[] right = new byte[halfBlockSize];
            byte[] temp;

            List<byte[]> internalKeys = generateInternalKeys();

            for (int x = (blockSize - 1); x >= 0; x--)
            {
                for (int i = 0; i < halfBlockSize; i++)
                {
                    left[i] = result[i];
                    right[i] = result[i + halfBlockSize];
                }
                temp = feistelCore(left, internalKeys[x], x);
                for (int i = 0; i < halfBlockSize; i++)
                {
                    byte tmp = left[i];
                    left[i] = (byte)(temp[i] ^ right[i]);
                    right[i] = tmp;
                }
                for (int i = 0; i < halfBlockSize; i++)
                {
                    result[i] = left[i];
                    result[i + halfBlockSize] = right[i];
                }
            }
            return result;
        }
        
        //Divide string s into blocks
        private byte[][] getBlocks(byte[] s) {
            int size = (s.Length % key.Length == 0) ? s.Length/key.Length : s.Length/key.Length + 1;
            byte[][] blocks = new byte[size][];
            int k = 0;
            for (int i = 0; i < size; i++)
            {
                blocks[i] = new byte[key.Length];
                for (int j = 0; j < key.Length; j++)
                {
                    blocks[i][j] = s[k];
                    k++;
                }
            }
            return blocks;
        }
        //XOR operation of 2 strings with same length
        private byte[] XOR(byte[] b1, byte[] b2) {
            byte[] res = new byte[b1.Length];
            for (int i = 0; i < b1.Length; i++) {
                res[i] += (byte)(b1[i] ^ (byte)b2[i]);
            }
            return res;
        }
        //iv generator
        private void generateIV(){
            byte[] res = new byte[key.Length];
            for (int i = 0; i < key.Length; i++)
            {
                if (i % 2 == 0)
                    res[i] = 0;
                else
                    res[i] = 1;
            }
            iv = res;
        }
        //ECB Mode
        private byte[][] EncryptECB(byte[][] blocks) {
            generateIV();
            byte[][] result = new byte[blocks.Length][];
            for (int i = 0; i < blocks.Length; i++) {
                result[i] = E(blocks[i]);
            }
            return result;
        }
        //CBC Mode
        private byte[][] EncryptCBC(byte[][] blocks)
        {
            generateIV();
            byte[][] result = new byte[blocks.Length][];
            result[0] = E(XOR(blocks[0], iv));
            for (int i = 1; i < blocks.Length; i++)
            {
                result[i] = E(XOR(blocks[i], result[i - 1]));
            }
            return result;
        }
        //CFB Mode
        private byte[][] EncryptCFB(byte[][] blocks) {
            generateIV();
            byte[][] result = new byte[blocks.Length][];
            result[0] = XOR(blocks[0], E(iv));
            for (int i = 1; i < blocks.Length; i++)
            {
                result[i] = XOR(blocks[i], E(result[i - 1]));
            }
            return result;
        }
        //OFB Mode
        private byte[][] EncryptOFB(byte[][] blocks) {
            generateIV();
            byte[][] result = new byte[blocks.Length][];
            byte[] temp = E(iv);
            for (int i = 0; i < blocks.Length; i++) { 
                result[i] = XOR(blocks[i], temp);
                temp = E(temp);
            }
            return result;
        }
        //General Encrypt
        public byte[] Encrypt(byte[] b, int mode) {
            int lastBLen = b.Length % key.Length;
            nPad = key.Length - lastBLen;
            byte[] plain;
            if (lastBLen != 0)
            { //needs padding
                plain = new byte[b.Length + key.Length - lastBLen];
                for (long i = 0; i < b.Length; i++)
                    plain[i] = b[i];
                for (long i = b.Length; i < plain.Length; i++)
                    plain[i] = (byte)0;
            }
            else {
                plain = b;
            }
            byte[][] blocks = getBlocks(plain);
            byte[][] cip = new byte[blocks.Length][];
            switch (mode) {
                case 1: cip = EncryptECB(blocks); break;
                case 2: cip = EncryptCBC(blocks); break;
                case 3: cip = EncryptCFB(blocks); break;
                case 4: cip = EncryptOFB(blocks); break;
            }
            //result format : blocksize + nPad + mode + IV + cip
            //blocksize to byte array
            byte[] blocksize = BitConverter.GetBytes(key.Length);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(blocksize);
            //nPad to byte array
            byte[] padded = BitConverter.GetBytes(nPad);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(padded);
            byte[] ciphered = new byte[blocks.Length * key.Length];
            for (int i = 0; i < blocks.Length; i++) {
                Array.Copy(cip[i], 0, ciphered, i * key.Length, key.Length);
            }
            byte[] result = new byte[blocksize.Length + padded.Length + 1 + iv.Length + ciphered.Length];
            Array.Copy(blocksize, result, blocksize.Length); //copy blocksize
            Array.Copy(padded, 0, result, blocksize.Length, padded.Length); //copy npad
            result[blocksize.Length + padded.Length] = (byte)mode; //copy mode
            Array.Copy(iv, 0, result, blocksize.Length + padded.Length + 1, iv.Length); //copy IV
            Array.Copy(ciphered, 0, result, blocksize.Length + padded.Length + 1 + iv.Length, ciphered.Length); //copy ciphered
            return result;
        }
        //Decrypt
        //Decrypt ECB
        private byte[][] DecryptECB(byte[][] blocks)
        {
            byte[][] result = new byte[blocks.Length][];
            for (int i = 0; i < blocks.Length; i++)
                result[i] = D(blocks[i]);
            return result;
        }
        //Decrypt CBC
        private byte[][] DecryptCBC(byte[][] blocks, byte[] IV)
        {
            byte[][] result = new byte[blocks.Length][];
            result[0] = XOR(D(blocks[0]), IV);
            for (int i = 1; i < blocks.Length; i++)
            {
                result[i] = XOR(D(blocks[i]), blocks[i - 1]);
            }
            return result;
        }
        //Decrypt CFB
        private byte[][] DecryptCFB(byte[][] blocks, byte[] IV)
        {
            byte[][] result = new byte[blocks.Length][];
            result[0] = XOR(blocks[0], E(IV));
            for (int i = 1; i < blocks.Length; i++)
            {
                result[i] = XOR(blocks[i], E(blocks[i - 1]));
            }
            return result;
        }
        //Decrypt OFB
        private byte[][] DecryptOFB(byte[][] blocks, byte[] IV)
        {
            byte[][] result = new byte[blocks.Length][];
            byte[] temp = E(IV);
            for (int i = 0; i < blocks.Length; i++)
            {
                result[i] = XOR(blocks[i], temp);
                temp = E(temp);
            }
            return result;
        }
        //Decrypt General
        public byte[] Decrypt(byte[] b) { 
            //parse the source byte
            //find the block size
            byte[] bsize = new byte[sizeof(int)];
            Array.Copy(b, 0, bsize, 0, sizeof(int));
            Array.Reverse(bsize);
            int blocksize = BitConverter.ToInt32(bsize,0);
            //find number of padded chars
            byte[] padsize = new byte[sizeof(int)];
            Array.Copy(b, bsize.Length, padsize, 0, padsize.Length);
            Array.Reverse(padsize);
            int padded = BitConverter.ToInt32(padsize, 0);
            //find the mode
            byte modebyte = b[bsize.Length + padsize.Length];
            short mode = (short)modebyte;
            //find the IV
            byte[] IV = new byte[blocksize];
            Array.Copy(b, bsize.Length + padsize.Length + 1, IV, 0, IV.Length);
            //take the ciphered bytes
            byte[] source = new byte[b.Length - IV.Length - 1 - padsize.Length - bsize.Length];
            Array.Copy(b, bsize.Length + padsize.Length + 1 + IV.Length, source, 0, source.Length);
            //Let's decrypt
            byte[][] blocks = getBlocks(source);
            byte[][] cip = new byte[blocks.Length][];
            switch (mode) {
                case 1: cip = DecryptECB(blocks); break;
                case 2: cip = DecryptCBC(blocks, IV); break;
                case 3: cip = DecryptCFB(blocks, IV); break;
                case 4: cip = DecryptOFB(blocks, IV); break;
            }
            byte[] ciphered = new byte[blocks.Length * key.Length];
            for (int j = 0; j < blocks.Length; j++)
            {
                Array.Copy(cip[j], 0, ciphered, j * key.Length, key.Length);
            }
            //discard padding bytes
            byte[] result = new byte[ciphered.Length - padded];
            Array.Copy(ciphered, result, ciphered.Length - padded);
            return result;
        }
    }
}
