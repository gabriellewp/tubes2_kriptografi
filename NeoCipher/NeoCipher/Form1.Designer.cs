﻿namespace NeoCipher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.modepanel = new System.Windows.Forms.Panel();
            this.bOFB = new System.Windows.Forms.RadioButton();
            this.bCFB = new System.Windows.Forms.RadioButton();
            this.bCBC = new System.Windows.Forms.RadioButton();
            this.bECB = new System.Windows.Forms.RadioButton();
            this.modecipher = new System.Windows.Forms.Label();
            this.keylabel = new System.Windows.Forms.Label();
            this.key = new System.Windows.Forms.TextBox();
            this.modelabel = new System.Windows.Forms.Label();
            this.encryptB = new System.Windows.Forms.RadioButton();
            this.decryptB = new System.Windows.Forms.RadioButton();
            this.textlabel = new System.Windows.Forms.Label();
            this.source = new System.Windows.Forms.TextBox();
            this.pathtext = new System.Windows.Forms.TextBox();
            this.go = new System.Windows.Forms.Button();
            this.browselabel = new System.Windows.Forms.Label();
            this.browse = new System.Windows.Forms.Button();
            this.cipherlabel = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveResult = new System.Windows.Forms.SaveFileDialog();
            this.loglabel = new System.Windows.Forms.Label();
            this.logtext = new System.Windows.Forms.TextBox();
            this.modepanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // modepanel
            // 
            this.modepanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modepanel.Controls.Add(this.bOFB);
            this.modepanel.Controls.Add(this.bCFB);
            this.modepanel.Controls.Add(this.bCBC);
            this.modepanel.Controls.Add(this.bECB);
            this.modepanel.Controls.Add(this.modecipher);
            this.modepanel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modepanel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.modepanel.Location = new System.Drawing.Point(12, 12);
            this.modepanel.Name = "modepanel";
            this.modepanel.Size = new System.Drawing.Size(149, 81);
            this.modepanel.TabIndex = 1;
            // 
            // bOFB
            // 
            this.bOFB.AutoSize = true;
            this.bOFB.Location = new System.Drawing.Point(85, 50);
            this.bOFB.Name = "bOFB";
            this.bOFB.Size = new System.Drawing.Size(52, 21);
            this.bOFB.TabIndex = 2;
            this.bOFB.TabStop = true;
            this.bOFB.Text = "OFB";
            this.bOFB.UseVisualStyleBackColor = true;
            this.bOFB.Click += new System.EventHandler(this.onOFBChecked);
            // 
            // bCFB
            // 
            this.bCFB.AutoSize = true;
            this.bCFB.Location = new System.Drawing.Point(85, 23);
            this.bCFB.Name = "bCFB";
            this.bCFB.Size = new System.Drawing.Size(50, 21);
            this.bCFB.TabIndex = 2;
            this.bCFB.TabStop = true;
            this.bCFB.Text = "CFB";
            this.bCFB.UseVisualStyleBackColor = true;
            this.bCFB.Click += new System.EventHandler(this.onCFBChecked);
            // 
            // bCBC
            // 
            this.bCBC.AutoSize = true;
            this.bCBC.Location = new System.Drawing.Point(1, 50);
            this.bCBC.Name = "bCBC";
            this.bCBC.Size = new System.Drawing.Size(52, 21);
            this.bCBC.TabIndex = 2;
            this.bCBC.TabStop = true;
            this.bCBC.Text = "CBC";
            this.bCBC.UseVisualStyleBackColor = true;
            this.bCBC.Click += new System.EventHandler(this.onCBCChecked);
            // 
            // bECB
            // 
            this.bECB.AutoSize = true;
            this.bECB.Location = new System.Drawing.Point(1, 24);
            this.bECB.Name = "bECB";
            this.bECB.Size = new System.Drawing.Size(50, 21);
            this.bECB.TabIndex = 3;
            this.bECB.TabStop = true;
            this.bECB.Text = "ECB";
            this.bECB.UseVisualStyleBackColor = true;
            this.bECB.Click += new System.EventHandler(this.onECBChecked);
            // 
            // modecipher
            // 
            this.modecipher.AutoSize = true;
            this.modecipher.Location = new System.Drawing.Point(3, 3);
            this.modecipher.Name = "modecipher";
            this.modecipher.Size = new System.Drawing.Size(87, 17);
            this.modecipher.TabIndex = 2;
            this.modecipher.Text = "Cipher Mode";
            // 
            // keylabel
            // 
            this.keylabel.AutoSize = true;
            this.keylabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keylabel.Location = new System.Drawing.Point(178, 16);
            this.keylabel.Name = "keylabel";
            this.keylabel.Size = new System.Drawing.Size(39, 17);
            this.keylabel.TabIndex = 2;
            this.keylabel.Text = "Key : ";
            // 
            // key
            // 
            this.key.Location = new System.Drawing.Point(223, 15);
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(260, 20);
            this.key.TabIndex = 3;
            // 
            // modelabel
            // 
            this.modelabel.AutoSize = true;
            this.modelabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelabel.Location = new System.Drawing.Point(178, 46);
            this.modelabel.Name = "modelabel";
            this.modelabel.Size = new System.Drawing.Size(53, 17);
            this.modelabel.TabIndex = 4;
            this.modelabel.Text = "Mode : ";
            // 
            // encryptB
            // 
            this.encryptB.AutoSize = true;
            this.encryptB.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.encryptB.Location = new System.Drawing.Point(237, 44);
            this.encryptB.Name = "encryptB";
            this.encryptB.Size = new System.Drawing.Size(72, 21);
            this.encryptB.TabIndex = 5;
            this.encryptB.TabStop = true;
            this.encryptB.Text = "Encrypt";
            this.encryptB.UseVisualStyleBackColor = true;
            this.encryptB.Click += new System.EventHandler(this.onEncryptChecked);
            // 
            // decryptB
            // 
            this.decryptB.AutoSize = true;
            this.decryptB.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decryptB.Location = new System.Drawing.Point(315, 44);
            this.decryptB.Name = "decryptB";
            this.decryptB.Size = new System.Drawing.Size(74, 21);
            this.decryptB.TabIndex = 6;
            this.decryptB.TabStop = true;
            this.decryptB.Text = "Decrypt";
            this.decryptB.UseVisualStyleBackColor = true;
            this.decryptB.Click += new System.EventHandler(this.onDecryptChecked);
            // 
            // textlabel
            // 
            this.textlabel.AutoSize = true;
            this.textlabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textlabel.Location = new System.Drawing.Point(11, 96);
            this.textlabel.Name = "textlabel";
            this.textlabel.Size = new System.Drawing.Size(88, 17);
            this.textlabel.TabIndex = 7;
            this.textlabel.Text = "Original Data";
            // 
            // source
            // 
            this.source.Location = new System.Drawing.Point(12, 116);
            this.source.Multiline = true;
            this.source.Name = "source";
            this.source.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.source.Size = new System.Drawing.Size(521, 130);
            this.source.TabIndex = 8;
            this.source.UseWaitCursor = true;
            // 
            // pathtext
            // 
            this.pathtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pathtext.Location = new System.Drawing.Point(119, 252);
            this.pathtext.Name = "pathtext";
            this.pathtext.ReadOnly = true;
            this.pathtext.Size = new System.Drawing.Size(311, 20);
            this.pathtext.TabIndex = 9;
            // 
            // go
            // 
            this.go.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.go.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go.Location = new System.Drawing.Point(408, 46);
            this.go.Name = "go";
            this.go.Size = new System.Drawing.Size(75, 23);
            this.go.TabIndex = 10;
            this.go.Text = "GO";
            this.go.UseVisualStyleBackColor = false;
            this.go.Click += new System.EventHandler(this.onGo);
            // 
            // browselabel
            // 
            this.browselabel.AutoSize = true;
            this.browselabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browselabel.Location = new System.Drawing.Point(11, 253);
            this.browselabel.Name = "browselabel";
            this.browselabel.Size = new System.Drawing.Size(88, 17);
            this.browselabel.TabIndex = 11;
            this.browselabel.Text = "Take from file";
            // 
            // browse
            // 
            this.browse.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.Location = new System.Drawing.Point(458, 252);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(75, 23);
            this.browse.TabIndex = 12;
            this.browse.Text = "Browse";
            this.browse.UseVisualStyleBackColor = true;
            this.browse.Click += new System.EventHandler(this.onBrowse);
            // 
            // cipherlabel
            // 
            this.cipherlabel.AutoSize = true;
            this.cipherlabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cipherlabel.Location = new System.Drawing.Point(12, 281);
            this.cipherlabel.Name = "cipherlabel";
            this.cipherlabel.Size = new System.Drawing.Size(95, 17);
            this.cipherlabel.TabIndex = 13;
            this.cipherlabel.Text = "Ciphered Data";
            // 
            // result
            // 
            this.result.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.result.Location = new System.Drawing.Point(12, 301);
            this.result.Multiline = true;
            this.result.Name = "result";
            this.result.ReadOnly = true;
            this.result.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.result.Size = new System.Drawing.Size(521, 130);
            this.result.TabIndex = 14;
            // 
            // save
            // 
            this.save.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.Location = new System.Drawing.Point(458, 437);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 15;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.onSave);
            // 
            // openFile
            // 
            this.openFile.FileName = "filesource";
            // 
            // loglabel
            // 
            this.loglabel.AutoSize = true;
            this.loglabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loglabel.Location = new System.Drawing.Point(16, 463);
            this.loglabel.Name = "loglabel";
            this.loglabel.Size = new System.Drawing.Size(31, 17);
            this.loglabel.TabIndex = 16;
            this.loglabel.Text = "Log";
            // 
            // logtext
            // 
            this.logtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logtext.Location = new System.Drawing.Point(12, 483);
            this.logtext.Multiline = true;
            this.logtext.Name = "logtext";
            this.logtext.ReadOnly = true;
            this.logtext.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logtext.Size = new System.Drawing.Size(521, 130);
            this.logtext.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 618);
            this.Controls.Add(this.logtext);
            this.Controls.Add(this.loglabel);
            this.Controls.Add(this.save);
            this.Controls.Add(this.result);
            this.Controls.Add(this.cipherlabel);
            this.Controls.Add(this.browse);
            this.Controls.Add(this.browselabel);
            this.Controls.Add(this.go);
            this.Controls.Add(this.pathtext);
            this.Controls.Add(this.source);
            this.Controls.Add(this.textlabel);
            this.Controls.Add(this.decryptB);
            this.Controls.Add(this.encryptB);
            this.Controls.Add(this.modelabel);
            this.Controls.Add(this.key);
            this.Controls.Add(this.keylabel);
            this.Controls.Add(this.modepanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NeoCipher";
            this.modepanel.ResumeLayout(false);
            this.modepanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel modepanel;
        private System.Windows.Forms.RadioButton bECB;
        private System.Windows.Forms.Label modecipher;
        private System.Windows.Forms.RadioButton bOFB;
        private System.Windows.Forms.RadioButton bCFB;
        private System.Windows.Forms.RadioButton bCBC;
        private System.Windows.Forms.Label keylabel;
        private System.Windows.Forms.TextBox key;
        private System.Windows.Forms.Label modelabel;
        private System.Windows.Forms.RadioButton encryptB;
        private System.Windows.Forms.RadioButton decryptB;
        private System.Windows.Forms.Label textlabel;
        private System.Windows.Forms.TextBox source;
        private System.Windows.Forms.TextBox pathtext;
        private System.Windows.Forms.Button go;
        private System.Windows.Forms.Label browselabel;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.Label cipherlabel;
        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.SaveFileDialog saveResult;
        private System.Windows.Forms.Label loglabel;
        private System.Windows.Forms.TextBox logtext;

    }
}

