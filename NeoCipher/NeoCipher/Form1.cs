﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeoCipher
{
    public partial class Form1 : Form
    {
        bool isECB = false; //true if ECB checked
        bool isCBC = false; //true if CBC checked
        bool isCFB = false; //true if CFB checked
        bool isOFB = false; //true if OFB checked
        bool isEncrypt = false; //true if Encrypt chosen
        bool isDecrypt = false; //true if Decrypt chosen
        byte[] plaintext = null;
        byte[] ciphertext = null;
        string fileext = ".txt"; //default extension
        public Form1()
        {
            InitializeComponent();
        }
        private bool isValidKey(string s) 
        {
            bool valid = true;
            int i = 0;
            while (valid && i < s.Length) {
                if ((48 <= s[i] && s[i] <= 57) || (65 <= s[i] && s[i] <= 90) || (97 <= s[i] && s[i] <= 122))
                    i++;
                else
                    valid = false;
            }
            return valid;
        }
        private void onECBChecked(Object o, EventArgs e) 
        {
            isECB = true;
            isCBC = isCFB = isOFB = false;
            logtext.Text += "ECB mode chosen" + Environment.NewLine;
        }
        private void onCBCChecked(Object o, EventArgs e)
        {
            isCBC = true;
            isECB = isCFB = isOFB = false;
            logtext.Text += "CBC mode chosen" + Environment.NewLine;
        }
        private void onCFBChecked(Object o, EventArgs e)
        {
            isCFB = true;
            isCBC = isECB = isOFB = false;
            logtext.Text += "CFB mode chosen" + Environment.NewLine;
        }
        private void onOFBChecked(Object o, EventArgs e)
        {
            isOFB = true;
            isCBC = isCFB = isECB = false;
            logtext.Text += "OFB mode chosen" + Environment.NewLine;
        }
        private void onEncryptChecked(Object o, EventArgs e)
        {
            isEncrypt = true;
            isDecrypt = false;
            bECB.Enabled = true;
            bCBC.Enabled = true;
            bCFB.Enabled = true;
            bOFB.Enabled = true;
            logtext.Text += "Encrypt mode chosen" + Environment.NewLine;
            logtext.Text += "Encryption mode enabled" + Environment.NewLine;
        }
        private void onDecryptChecked(Object o, EventArgs e)
        {
            isDecrypt = true;
            isEncrypt = false;
            bECB.Enabled = false;
            bCBC.Enabled = false;
            bCFB.Enabled = false;
            bOFB.Enabled = false;
            logtext.Text += "Decrypt mode chosen" + Environment.NewLine;
            logtext.Text += "Encryption mode disabled" + Environment.NewLine;
        }
        private void onGo(Object o, EventArgs e) {
            bool modeChosen = isCBC || isCFB || isECB || isOFB;
            bool actChosen = isEncrypt || isDecrypt;
            if (key.Text == string.Empty || plaintext == null || !actChosen || !modeChosen)
            {
                MessageBox.Show("Complete the form first!" ,"Incomplete form");
            } else if (!isValidKey(key.Text) || key.Text.Length < 8){
                MessageBox.Show("The key must be at least 8 characters and only contain alphanumerics" ,"Invalid key");
                logtext.Text += isValidKey(key.Text) + Environment.NewLine;
            }
            else {
                Cipher c = new Cipher(key.Text);
                logtext.Text += "Ciphering started" + Environment.NewLine;
                if (isEncrypt && isECB) {
                    ciphertext = c.Encrypt(plaintext, 1);
                }
                else if (isEncrypt && isCBC) {
                    ciphertext = c.Encrypt(plaintext, 2);
                }
                else if (isEncrypt && isCFB) {
                    ciphertext = c.Encrypt(plaintext, 3);
                }
                else if (isEncrypt && isOFB) {
                    ciphertext = c.Encrypt(plaintext, 4);
                }
                else if (isDecrypt) {
                    ciphertext = c.Decrypt(plaintext);
                }
                logtext.Text += "Ciphering finished" + Environment.NewLine;
                StringBuilder sb = new StringBuilder();
                foreach (byte b in ciphertext)
                    sb.Append((char)b);
                result.Text = sb.Replace("\0", "").ToString();
            }
        }
        private void onBrowse(Object o, EventArgs e) {
            DialogResult d = openFile.ShowDialog();
            string filename = "";
            if (d == DialogResult.OK) {
                string file = openFile.FileName;
                try{
                    filename = file.ToString();
                    plaintext = File.ReadAllBytes(file);
                    StringBuilder sb = new StringBuilder();
                    foreach (byte b in plaintext)
                        sb.Append((char)b);
                    source.Text = sb.Replace("\0", "").ToString();
                    pathtext.Text = filename;
                    fileext = filename.Split('.')[1];
                    logtext.Text += "Source file : " + filename + Environment.NewLine;
                    logtext.Text += "Source length : " + plaintext.Length + Environment.NewLine;
                }
                catch (IOException) { }
            }
        }
        private void onSave(Object o, EventArgs e) {
            DialogResult d = saveResult.ShowDialog();
            if (d == DialogResult.OK) {
                string filename = saveResult.FileName;
                if (!filename.Contains("."))
                    filename += "." + fileext;
                using (FileStream fs = new FileStream(filename, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        foreach (byte b in ciphertext)
                            bw.Write(b);
                        bw.Close();
                    }
                }
            }
        }
    }
}
