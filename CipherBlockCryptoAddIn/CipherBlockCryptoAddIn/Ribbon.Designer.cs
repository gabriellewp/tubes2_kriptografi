﻿namespace CipherBlockCryptoAddIn
{
    partial class Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCryptography = this.Factory.CreateRibbonTab();
            this.groupEncrypt = this.Factory.CreateRibbonGroup();
            this.buttonEncrypt = this.Factory.CreateRibbonButton();
            this.buttonDecrypt = this.Factory.CreateRibbonButton();
            this.tabCryptography.SuspendLayout();
            this.groupEncrypt.SuspendLayout();
            // 
            // tabCryptography
            // 
            this.tabCryptography.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabCryptography.Groups.Add(this.groupEncrypt);
            this.tabCryptography.Label = "Cryptography";
            this.tabCryptography.Name = "tabCryptography";
            // 
            // groupEncrypt
            // 
            this.groupEncrypt.Items.Add(this.buttonEncrypt);
            this.groupEncrypt.Items.Add(this.buttonDecrypt);
            this.groupEncrypt.Label = "Block Cipher";
            this.groupEncrypt.Name = "groupEncrypt";
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonEncrypt.Image = global::CipherBlockCryptoAddIn.Properties.Resources.encrypt;
            this.buttonEncrypt.Label = "Encrypt";
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.ShowImage = true;
            this.buttonEncrypt.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonEncrypt_Click);
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonDecrypt.Image = global::CipherBlockCryptoAddIn.Properties.Resources.decrypt;
            this.buttonDecrypt.Label = "Decrypt";
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.ShowImage = true;
            this.buttonDecrypt.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDecrypt_Click);
            // 
            // Ribbon
            // 
            this.Name = "Ribbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tabCryptography);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_Load);
            this.tabCryptography.ResumeLayout(false);
            this.tabCryptography.PerformLayout();
            this.groupEncrypt.ResumeLayout(false);
            this.groupEncrypt.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabCryptography;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupEncrypt;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonEncrypt;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDecrypt;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon Ribbon
        {
            get { return this.GetRibbon<Ribbon>(); }
        }
    }
}
