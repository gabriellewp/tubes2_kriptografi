﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Diagnostics;
namespace CipherBlockCryptoAddIn
{
    public partial class Ribbon
    {
        String plainText;
        String encryptText;
        String decryptText;
        String keyText;
        byte[] arrayBytePlainText;
        byte[] arrayByteKey;
        byte[] arrayByteCipherText;
        private void Ribbon_Load(object sender, RibbonUIEventArgs e)
        {
            
        }

        private void buttonEncrypt_Click(object sender, RibbonControlEventArgs e)
        {
            
            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
            document.ActiveWindow.Selection.WholeStory();
            document.ActiveWindow.Selection.Copy();
            IDataObject data = Clipboard.GetDataObject();
            plainText = data.GetData(DataFormats.Text).ToString();
            Debug.WriteLine("plaintext"+plainText);
            blockCipherEncrypt();
            StringBuilder sb = new StringBuilder();
            foreach (byte b in arrayByteCipherText)
                sb.Append((char)b);
            document.ActiveWindow.Selection.Text = sb.Replace("\0", "").ToString();
            //document.ActiveWindow.Text = Encoding.ASCII.GetString(arrayByteCipherText);
            Debug.WriteLine("ciphertext : "+Encoding.ASCII.GetString(arrayByteCipherText));
        }
        private void buttonDecrypt_Click(object sender, RibbonControlEventArgs e)
        {
            GC.Collect(0);
            Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;

            document.ActiveWindow.Selection.WholeStory();
            document.ActiveWindow.Selection.Copy();
            IDataObject data = Clipboard.GetDataObject();
            encryptText = data.GetData(DataFormats.Text).ToString();
            Debug.WriteLine("encrypttext" +encryptText);
            blockCipherDecrypt();
            StringBuilder sb = new StringBuilder();
            foreach (byte b in arrayBytePlainText)
                sb.Append((char)b);
            document.ActiveWindow.Selection.Text = sb.Replace("\0", "").ToString();
        }
        private void blockCipherEncrypt(){            
            String keyText = "abcdefghabcdefgh";
            Cipher cipher = new Cipher(keyText);
            arrayBytePlainText = Encoding.ASCII.GetBytes(plainText);
            arrayByteCipherText = cipher.Encrypt(arrayBytePlainText);
        }
        private void blockCipherDecrypt()
        {
            String keyText = "abcdefghabcdefgh";
            Cipher cipher = new Cipher(keyText);
            arrayByteCipherText = Encoding.ASCII.GetBytes(encryptText);
            arrayBytePlainText = cipher.Decrypt(arrayByteCipherText);
        }

        
    }
}
