﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherBlockCryptoAddIn
{
    class ByteManipulator
    {
        public static bool GetBit(byte source, int idx)
        {
            return (((source >> idx) & 1) == 1);
        }

        public static byte SetBit(byte target, bool source, int idx)
        {
            int value = source ? 1 : 0;
            int temp = (value << idx);
            return (byte)((target & ~(1 << idx)) | temp);
        }

        public static byte CircularLeftShift(byte toShift, int count)
        {

            int actualCount = count % 8;
            if (actualCount < 0)
            {
                actualCount += 8;
            }
            byte finalResult = (byte)(toShift << actualCount);
            for (int i = 0; i < actualCount; i++)
            {
                finalResult = SetBit(finalResult, GetBit(toShift, 7 - i), actualCount - 1 - i);
            }
            return finalResult;
        }

        public static byte CircularRightShift(byte toShift, int count)
        {
            int actualCount = count % 8;
            if (actualCount < 0)
            {
                actualCount += 8;
            }
            byte finalResult = (byte)(toShift >> actualCount);
            for (int i = 0; i < actualCount; i++)
            {
                finalResult = SetBit(finalResult, GetBit(toShift, actualCount - 1 - i), 7 - i);
            }
            return finalResult;
        }
    }
}
